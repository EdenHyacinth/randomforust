extern crate criterion;
extern crate lyssarian_forest;

use std::sync::RwLock;

use criterion::black_box;
use criterion::Criterion;

use lyssarian_forest::tree_components::data_store::{DataStore, ElementType};
use lyssarian_forest::tree_components::tree::{Branch, evaluate_tree};

fn plant_tree(all_data: &DataStore, weight_lock: &[f32]) -> Branch {
    all_data.plant_tree(&weight_lock).grow(all_data)
}

fn eval_tree(all_data: &DataStore, tree: Vec<Branch>) -> f64 {
    let (res, _) = evaluate_tree(tree, &all_data);
    res
}

fn plant_and_eval_tree(all_data: &DataStore, weight_lock: &[f32]) -> f64 {
    let tree = all_data.plant_tree(&weight_lock).grow(all_data);
    let (res, _) = evaluate_tree(vec![tree], &all_data);
    res
}

fn plant_many_trees(all_data: &DataStore, n_rounds: usize, tree_per_round: usize) {
    let mut tree_vec: Vec<Branch> = Vec::with_capacity(1000);
    let weight_lock: RwLock<Vec<f32>> = RwLock::new(vec![1_f32; all_data.total_rows]);
    for _ in 0..n_rounds {
        let mut weights_update: Vec<f32> = Vec::with_capacity(all_data.total_rows);
        for _ in 0..tree_per_round {
            let tree = all_data
                .plant_tree(&*weight_lock.read().unwrap())
                .grow(&all_data);
            tree_vec.push(tree);
            let (_, weights) = evaluate_tree(tree_vec.clone(), &all_data);
            for (element, new_weight) in weights_update.iter_mut().zip(weights.iter()) {
                *element += *new_weight
            }
        }
        {
            let mut writer = weight_lock.write().unwrap();
            for (element, new_weight) in writer.iter_mut().zip(weights_update.iter()) {
                *element += *new_weight
            }
        }
    }
}

fn plant_tree_bench(c: &mut Criterion) {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );
    let lock = vec![1_f32; all_data.total_rows];
    c.bench_function("Plant Tree", move |b| {
        b.iter(|| plant_tree(black_box(&all_data), &lock))
    });
}

fn eval_tree_bench(c: &mut Criterion) {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );
    let lock = vec![1_f32; all_data.total_rows];
    let tree = vec![all_data.plant_tree(&lock).grow(&all_data)];
    c.bench_function("Evaluate Tree", move |b| {
        b.iter(|| eval_tree(black_box(&all_data), tree.clone()))
    });
}

fn create_tree_bench(c: &mut Criterion) {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );
    let lock = vec![1_f32; all_data.total_rows];
    c.bench_function("Plant & Eval Tree", move |b| {
        b.iter(|| plant_and_eval_tree(black_box(&all_data), &lock))
    });
}

fn create_many_trees(c: &mut Criterion) {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );
    c.bench_function("Plant & Eval 50 Trees", move |b| {
        b.iter(|| plant_many_trees(black_box(&all_data), 5, 10))
    });
}

fn main() {
    let mut criterion = Criterion::default().configure_from_args().sample_size(35);
    plant_tree_bench(&mut criterion); // 700us (780 with par)
    eval_tree_bench(&mut criterion); // 300us (350 with par)
    create_tree_bench(&mut criterion); // 1000us (1200us with par)
    create_many_trees(&mut criterion);
    // Par Iter - 750ms
    // iter - 75ms
    criterion.final_summary();
}
