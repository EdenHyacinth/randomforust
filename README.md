# RandomForest Implementation in Rust 

Initial implementation of a Random Forest model in Rust. This is designed purely for interest in how one would be put together, and I would strongly encourage against using it in any kind of production, at least until we can publish *some* performance statistics.

## Datasets Used 

* **Banknote Authentication Dataset**

Thanks given to Dua, D. and Karra Taniskidou, E. (2017). UCI Machine Learning Repository. (http://archive.ics.uci.edu/ml),
as well as Helene Doerksen (University of Applied Sciences, Ostwestfalen-Lippe), who provided the dataset to the UCI ML Repository, available [here](https://archive.ics.uci.edu/ml/datasets/banknote+authentication). 