use tree_components::data_store::DataStore;
use tree_components::tree::{Branch, RowIdxMapping};
use utils::column_tools::most_frequent_value;
use utils::median::NumericAggregation;

const LEARNING_RATE: f32 = 0.15;

#[derive(Clone)]
pub struct TreeResult<'a, 'b, ResultType> {
    total_rows: usize,
    current_row: usize,
    results: &'b [ResultType],
    root_node: Vec<Branch>,
    data_store: &'a DataStore,
}

impl<'a, 'b, ResultType> TreeResult<'a, 'b, ResultType> {
    pub fn new(
        data_store: &'a DataStore,
        results: &'b [ResultType],
        root_node: Vec<Branch>,
    ) -> Self {
        TreeResult {
            total_rows: data_store.total_rows,
            current_row: 0,
            results,
            root_node,
            data_store,
        }
    }
}

impl<'a, 'b> Iterator for TreeResult<'a, 'b, f32> {
    type Item = (f32, f32);
    fn next(&mut self) -> Option<Self::Item> {
        if self.current_row < self.total_rows {
            let x = Some((
                self.results[self.current_row],
                (self
                    .root_node
                    .iter()
                    .map(|root_node| match root_node {
                        Branch::NextNode(ref node) => {
                            match node.lookup_element(self.data_store, self.current_row) {
                                RowIdxMapping::Float(float_elem) => float_elem,
                                _ => unreachable!(),
                            }
                        }
                        Branch::Leaf(result) => match result.clone() {
                            RowIdxMapping::Float(float_elem) => float_elem,
                            _ => unreachable!(),
                        },
                    })
                    .collect::<Vec<f32>>())
                    .sketched_median(
                    self.results.len(),
                    &((0..self.results.len()).collect::<Vec<usize>>()),
                    ),
            ));
            self.current_row += 1;
            x
        } else {
            None
        }
    }
}

impl<'a, 'b> Iterator for TreeResult<'a, 'b, i32> {
    type Item = (i32, i32);
    fn next(&mut self) -> Option<Self::Item> {
        if self.current_row < self.total_rows {
            let x = Some((
                self.results[self.current_row],
                (self
                    .root_node
                    .iter()
                    .map(|root_node| match root_node {
                        Branch::NextNode(ref node) => {
                            match node.lookup_element(self.data_store, self.current_row) {
                                RowIdxMapping::Ordinal(ord_elem) => ord_elem,
                                _ => unreachable!(),
                            }
                        }
                        Branch::Leaf(result) => match result.clone() {
                            RowIdxMapping::Ordinal(ord_elem) => ord_elem,
                            _ => unreachable!(),
                        },
                    })
                    .collect::<Vec<i32>>())
                    .sketched_median(
                        self.total_rows,
                        &((0..self.data_store.total_rows).collect::<Vec<usize>>()),
                    ),
            ));
            self.current_row += 1;
            x
        } else {
            None
        }
    }
}

impl<'a, 'b> Iterator for TreeResult<'a, 'b, usize> {
    type Item = (usize, usize);
    fn next(&mut self) -> Option<Self::Item> {
        if self.current_row < self.total_rows {
            let x = Some((
                self.results[self.current_row],
                most_frequent_value(
                    &self
                        .root_node
                        .iter()
                        .map(|root_node| match root_node {
                            Branch::NextNode(ref node) => {
                                match node.lookup_element(self.data_store, self.current_row) {
                                    RowIdxMapping::Categorical(cat_elem) => cat_elem,
                                    _ => unreachable!(),
                                }
                            }
                            Branch::Leaf(result) => match result.clone() {
                                RowIdxMapping::Categorical(cat_elem) => cat_elem,
                                _ => unreachable!(),
                            },
                        })
                        .collect::<Vec<usize>>(),
                    &((0..self.data_store.total_rows).collect::<Vec<usize>>()),
                ),
            ));
            self.current_row += 1;
            x
        } else {
            None
        }
    }
}

impl<'a, 'b> Iterator for TreeResult<'a, 'b, bool> {
    type Item = (bool, bool);
    fn next(&mut self) -> Option<Self::Item> {
        if self.current_row < self.total_rows {
            let x = Some((
                self.results[self.current_row],
                (self
                    .root_node
                    .iter()
                    .map(|root_node| match root_node {
                        Branch::NextNode(ref node) => {
                            match node.lookup_element(self.data_store, self.current_row) {
                                RowIdxMapping::Boolean(bool_elem) => bool_elem,
                                _ => unreachable!(),
                            }
                        }
                        Branch::Leaf(result) => match result.clone() {
                            RowIdxMapping::Boolean(bool_elem) => bool_elem,
                            _ => unreachable!(),
                        },
                    })
                    .fold(0_usize, |acc, elem| if elem { acc + 1 } else { acc }))
                    > self.root_node.len() / 2,
            ));
            self.current_row += 1;
            x
        } else {
            None
        }
    }
}

pub trait Evaluation {
    fn evaluate(self) -> (f64, Vec<f32>);
}

impl<'a, 'b> Evaluation for TreeResult<'a, 'b, bool> {
    fn evaluate(self) -> (f64, Vec<(f32)>) {
        balanced_accuracy(self)
    }
}

impl<'a, 'b> Evaluation for TreeResult<'a, 'b, usize> {
    fn evaluate(self) -> (f64, Vec<(f32)>) {
        unimplemented!()
    }
}

impl<'a, 'b> Evaluation for TreeResult<'a, 'b, i32> {
    fn evaluate(self) -> (f64, Vec<(f32)>) {
        unimplemented!()
    }
}

impl<'a, 'b> Evaluation for TreeResult<'a, 'b, f32> {
    fn evaluate(self) -> (f64, Vec<(f32)>) {
        mean_squared_error(self)
    }
}

fn mean_squared_error<'a, 'b>(tree: TreeResult<'a, 'b, f32>) -> (f64, Vec<f32>)
    where
        TreeResult<'a, 'b, f32>: Iterator<Item=(f32, f32)>,
{
    let rows = tree.total_rows as f32;
    let error =
        tree.map(|(prediction, actual)| (prediction - actual) * (prediction - actual) / rows);
    let max_scale = error.clone().fold(core::f32::MIN, f32::max);
    let weight = error
        .clone()
        .map(|mse| (mse / max_scale))
        .collect::<Vec<f32>>();
    let error = error.fold(0., |acc, elem| acc + elem) / rows;
    (error.into(), weight)
}

fn balanced_accuracy<'a, 'b>(tree: TreeResult<'a, 'b, bool>) -> (f64, Vec<(f32)>)
where
    TreeResult<'a, 'b, bool>: Iterator<Item=(bool, bool)>,
{
    let mut true_positive: usize = 0;
    let mut positives: usize = 0;
    let mut true_negative: usize = 0;
    let mut negatives: usize = 0;
    let mut weight: Vec<f32> = Vec::with_capacity(tree.total_rows);

    for (actual, prediction) in tree {
        match (prediction, actual) {
            (true, true) => {
                true_positive += 1;
                positives += 1;
                weight.push(0_f32);
            }
            (true, _) => {
                positives += 1;
                weight.push(LEARNING_RATE);
            }
            (false, true) => {
                negatives += 1;
                weight.push(LEARNING_RATE);
            }
            (_, _) => {
                true_negative += 1;
                negatives += 1;
                weight.push(0_f32);
            }
        }
    }

    /*dbg!(true_positive);
    dbg!(positives);
    dbg!(negatives);
    dbg!(true_negative);
    */

    (
        ((true_positive as f64 / positives as f64) + (true_negative as f64 / negatives as f64))
            / 2.0_f64,
        weight,
    )
}
