pub trait NumericAggregation {
    type Input;
    type Output;

    fn sketched_median(&self, data_len: usize, sketch_indices: &[usize]) -> Self::Input
        where
            std::vec::Vec<Self::Input>: std::clone::Clone,
            Self::Input: std::cmp::PartialOrd,
            Self::Input: Clone,
            Self::Input: std::default::Default,
            Self::Input: std::convert::From<u8>;

    fn min_max(&self, sketch_indices: &[usize]) -> (Self::Input, Self::Input)
        where
            Self::Input: std::cmp::PartialOrd;
}

impl NumericAggregation for Vec<i32> {
    type Input = i32;
    type Output = f32;

    fn sketched_median(&self, data_len: usize, sketch_indices: &[usize]) -> Self::Input {
        let median_data = sketch_indices.iter().map(|index| self[*index]).collect::<Vec<i32>>();
        select(&median_data, data_len / 2 - 1)
    }

    fn min_max(&self, sketch_indices: &[usize]) -> (Self::Input, Self::Input) {
        let mut max = self[sketch_indices[0]];
        let mut min = max;
        for idx in sketch_indices {
            let x = self[*idx];
            if x > max {
                max = x
            }
            if x < min {
                min = x
            }
        }
        (min, max)
    }
}

impl NumericAggregation for Vec<f32> {
    type Input = f32;
    type Output = f32;

    fn sketched_median(&self, data_len: usize, sketch_indices: &[usize]) -> Self::Input {
        let median_data = sketch_indices.iter().map(|index| self[*index]).collect::<Vec<f32>>();
        select(&median_data, data_len / 2 - 1)
    }

    fn min_max(&self, sketch_indices: &[usize]) -> (Self::Input, Self::Input) {
        let mut max = self[sketch_indices[0]];
        let mut min = max;
        for idx in sketch_indices {
            let x = self[*idx];
            if x > max {
                max = x
            }
            if x < min {
                min = x
            }
        }
        (min, max)
    }
}

fn median_of_median<T>(med_array: &[T], _exp_partition: usize) -> T
    where
        std::vec::Vec<T>: std::clone::Clone,
        T: std::cmp::PartialOrd,
        T: Clone,
        T: std::fmt::Debug,
        T: std::default::Default,
        T: std::ops::Add<Output=T>,
        T: std::ops::Div<Output=T>,
        T: std::convert::From<u8>,
{
    let len_array = med_array.len();
    if len_array <= 5 {
        quick_median(med_array, len_array)
    } else {
        let mut median_of_medians: Vec<T> = Vec::with_capacity(len_array / 5);
        for i in 0_usize..=((len_array - 1) / 5) {
            let remaining_elements = len_array - 5 * i;
            let next_iter = std::cmp::min(remaining_elements, 5);
            let mut subset_array: Vec<T> = Vec::with_capacity(next_iter);
            for j in 0_usize..next_iter {
                subset_array.push(med_array[(i * 5 + j)].clone());
            }
            median_of_medians.push(quick_median(&subset_array, next_iter));
        }
        median_of_median(&median_of_medians, median_of_medians.len() / 2)
    }
}

fn select<T>(median_array: &[T], pivot_element: usize) -> T
    where
        std::vec::Vec<T>: std::clone::Clone,
        T: std::cmp::PartialOrd,
        T: Clone,
        T: std::fmt::Debug,
        T: std::default::Default,
        T: std::ops::Add<Output=T>,
        T: std::ops::Div<Output=T>,
        T: std::convert::From<u8>,
{
    let len_array = median_array.len();
    let partition: &T = &median_of_median(&median_array, len_array / 2);
    let mut set_1: Vec<T> = Vec::with_capacity(len_array / 2);
    let mut set_2: Vec<T> = Vec::with_capacity(len_array / 2);
    let mut set_3: Vec<T> = Vec::with_capacity(len_array / 2);

    for elem in median_array {
        if elem < partition {
            set_1.push(elem.clone());
        } else if elem == partition {
            set_2.push(elem.clone());
        } else {
            set_3.push(elem.clone());
        }
    }

    if pivot_element < set_1.len() {
        select(&set_1, pivot_element)
    } else if pivot_element + 1 > set_1.len() + set_2.len() {
        select(&set_3, pivot_element - set_1.len() - set_2.len())
    } else {
        partition.clone()
    }
}

fn quick_median<T>(median_array: &[T], len_array: usize) -> T
    where
        std::vec::Vec<T>: std::clone::Clone,
        T: std::cmp::PartialOrd,
        T: Clone,
        T: std::fmt::Debug,
        T: std::default::Default,
        T: std::ops::Add<Output=T>,
        T: std::ops::Div<Output=T>,
        T: std::convert::From<u8>,
{
    let mut median_array_scratch: Vec<T> = Vec::with_capacity(len_array);
    for elem in median_array {
        median_array_scratch.push(elem.clone());
    }
    median_array_scratch.sort_by(|a, b| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal));
    if len_array == 2 {
        (median_array_scratch[0].clone() + median_array_scratch[1].clone())
            / std::convert::From::from(2_u8)
    } else if len_array % 2 == 0 {
        (median_array_scratch[len_array / 2].clone()
            + median_array_scratch[(len_array + 1) / 2].clone())
            / std::convert::From::from(2)
    } else {
        median_array_scratch[len_array / 2].clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn median_of_40() {
        let example_vec = vec![
            102, 103, 104, 101, 103, 101, 101, 101, 104, 104, 103, 104, 102, 101, 100, 101, 100,
            103, 103, 104, 104, 101, 102, 103, 100, 101, 104, 102, 101, 101, 104, 102, 102, 104,
            101, 100, 100, 103, 104, 104,
        ];
        assert_eq!(102, select(&example_vec, 19))
    }

    #[test]
    fn median_of_39() {
        let example_vec = vec![
            650, 468, 957, 709, 465, 414, 404, 226, 414, 163, 150, 773, 276, 975, 780, 870, 107,
            365, 893, 344, 488, 946, 270, 932, 154, 518, 546, 1007, 858, 826, 574, 449, 525, 657,
            402, 535, 925, 875, 520,
        ];
        assert_eq!(525, select(&example_vec, 19))
    }
}
