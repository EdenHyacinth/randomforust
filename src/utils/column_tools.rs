use std::collections::HashMap;

use tree_components::data_store::ColumnType;
use tree_components::tree::RowIdxMapping;
use utils::median::NumericAggregation;

pub fn map_data<'a, T: 'static, A: 'static, B: 'static>(
    data: &'a [T],
    indexes: &'a [usize],
    map_fn: A,
) -> Box<dyn Iterator<Item=B> + 'a>
    where
        A: Fn(T) -> B,
        T: std::clone::Clone,
{
    Box::new(
        indexes
            .iter()
            .map(move |index| map_fn(data[*index].clone())),
    )
}

pub fn fold_data<T: 'static, A, B>(data: &[T], indexes: &[usize], fold: A, initialise: B) -> T
    where
        A: Fn(T, T) -> T,
        B: FnOnce() -> T,
        T: std::clone::Clone,
{
    let mut state = initialise();
    for idx in indexes {
        state = fold(state, data[*idx].clone());
    }
    state
}

pub fn average_column_value(data: &ColumnType, row_indices: &[usize]) -> RowIdxMapping {
    match data {
        ColumnType::Boolean(column_data) => RowIdxMapping::Boolean(
            row_indices
                .iter()
                .filter(|index| column_data[**index])
                .count()
                > (row_indices.len() / 2),
        ),
        ColumnType::Categorical(_column_data) => unimplemented!(),
        ColumnType::Float(column_data) => {
            RowIdxMapping::Float(column_data.sketched_median(row_indices.len(), &row_indices))
        }
        ColumnType::Ordinal(column_data) => {
            RowIdxMapping::Ordinal(column_data.sketched_median(row_indices.len(), &row_indices))
        }
    }
}

pub fn most_frequent_value(column_data: &[usize], row_indices: &[usize]) -> usize {
    let mut unique_values = row_indices
        .iter()
        .map(|index| column_data[*index])
        .collect::<Vec<usize>>();
    unique_values.sort();
    unique_values.dedup();

    let mut element_index: HashMap<usize, usize> = HashMap::new();

    for (index, element) in unique_values.iter().enumerate() {
        element_index.insert(*element, index);
    }

    let mut count_by_element: Vec<usize> = vec![0_usize; unique_values.len()];

    for row_index in row_indices {
        count_by_element[element_index[&column_data[*row_index]]] += 1;
    }

    let mut max_elem: usize = 0;
    let mut max_pos: usize = 0;
    for (index, value) in count_by_element.iter().enumerate() {
        if *value > max_elem {
            max_elem = *value;
            max_pos = index;
        }
    }
    unique_values[max_pos]
}

#[cfg(test)]
mod tests {
    //use super::*;
}
