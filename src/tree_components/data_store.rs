use std::fs::File;
use std::io::{self, BufReader};
use std::io::prelude::*;
use std::sync::RwLock;

use lazy_static::lazy_static;
use rayon::prelude::*;
use regex::{Regex, RegexBuilder};

use tree_components::tree::{Branch, evaluate_tree};

use super::tree::Tree;

#[derive(Clone, Debug)]
pub enum ColumnType {
    Float(Vec<f32>),
    Ordinal(Vec<i32>),
    Categorical(Vec<usize>),
    Boolean(Vec<bool>),
}

lazy_static! {
    static ref NUMERIC_REGEX: Regex = Regex::new(r"[0-9]").unwrap();
    static ref FLOAT_REGEX: Regex = Regex::new(r"[0-9]\.[0-9]").unwrap();
    static ref CHARACTER_REGEX: Regex = Regex::new(r"[a-zA-Z]").unwrap();
    static ref BOOLEAN_REGEX: Regex = RegexBuilder::new(r"true|false")
        .case_insensitive(true)
        .build()
        .unwrap();
}

const TREE_PER_ROUND: usize = 10;
const N_ROUNDS: usize = 10;
const ROW_SAMPLING: usize = 200;

pub struct DataStore {
    /// Immutable Data Structure to store the initial data input.
    /// Presently stores all data provided, and is limited to max_usize elements.
    _headers: Vec<String>,
    pub data: Vec<ColumnType>,
    pub result_index: usize,
    pub results: ColumnType,
    pub total_rows: usize,
    pub total_columns: usize,
}

impl DataStore {
    pub fn new(
        column_type_list: Vec<ElementType>,
        result_index: usize,
        file_name: String,
        header_row: bool,
    ) -> DataStore {
        fn read(
            file_name: String,
            idx_mapping: &[ElementType],
            header_row: bool,
            result_column_index: usize,
        ) -> Result<(usize, Vec<ColumnType>, ColumnType), io::Error> {
            let f = File::open(file_name)?;
            let mut f = BufReader::new(f);
            let mut header_line = String::new();
            let mut idx_tracker;
            // Might need interior mutability?
            let mut results = match idx_mapping[result_column_index] {
                ElementType::Boolean => ColumnType::Boolean(Vec::new()),
                ElementType::Categorical => ColumnType::Categorical(Vec::new()),
                ElementType::Float => ColumnType::Float(Vec::new()),
                ElementType::Ordinal => ColumnType::Ordinal(Vec::new()),
            };
            let mut data = idx_mapping
                .iter().enumerate()
                .filter(|(index, _)| *index != result_column_index)
                .map(|(_, column_type)| match column_type {
                    ElementType::Float => ColumnType::Float(Vec::new()),
                    ElementType::Categorical => ColumnType::Categorical(Vec::new()),
                    ElementType::Ordinal => ColumnType::Ordinal(Vec::new()),
                    ElementType::Boolean => ColumnType::Boolean(Vec::new()),
                })
                .collect::<Vec<ColumnType>>();
            let mut total_rows = 0;
            // Pull first line for header;
            if header_row {
                let _ = f.read_line(&mut header_line);
            }
            for line in f.lines() {
                idx_tracker = 0;
                for (index, elem) in line.unwrap().split(',').enumerate() {
                    let column_index: usize = index % idx_mapping.len();
                    if column_index == result_column_index {
                        match results {
                            ColumnType::Boolean(ref mut column_data) => {
                                column_data.push(elem.to_ascii_lowercase().parse::<bool>().unwrap())
                            }
                            ColumnType::Categorical(ref mut column_data) => {
                                column_data.push(elem.parse::<usize>().unwrap())
                            }
                            ColumnType::Float(ref mut column_data) => {
                                column_data.push(elem.parse::<f32>().unwrap())
                            }
                            ColumnType::Ordinal(ref mut column_data) => {
                                column_data.push(elem.parse::<i32>().unwrap())
                            }
                        }
                    } else {
                        match &idx_mapping[idx_tracker] {
                            ElementType::Float => match data[column_index] {
                                ColumnType::Float(ref mut float_data) => {
                                    float_data.push(elem.parse::<f32>().unwrap())
                                }
                                _ => panic!("Column Index Map did not match Found Columns"),
                            },
                            ElementType::Categorical => match data[column_index] {
                                ColumnType::Categorical(ref mut categorical_data) => {
                                    categorical_data.push(elem.parse::<usize>().unwrap())
                                }
                                _ => panic!("Column Index Map did not match Found Columns"),
                            },
                            ElementType::Ordinal => match data[column_index] {
                                ColumnType::Ordinal(ref mut ordinal_data) => {
                                    ordinal_data.push(elem.parse::<i32>().unwrap())
                                }
                                _ => panic!("Column Index Map did not match Found Columns"),
                            },
                            ElementType::Boolean => match data[column_index] {
                                ColumnType::Boolean(ref mut boolean_data) => boolean_data
                                    .push(elem.to_ascii_lowercase().parse::<bool>().unwrap()),
                                _ => panic!("Column Index Map did not match Found Columns"),
                            },
                        }
                    };
                    idx_tracker += 1;
                }
                total_rows += 1;
            }
            Ok((total_rows, data, results))
        }

        let (total_rows, data, results) =
            read(file_name, &column_type_list, header_row, result_index).unwrap();

        DataStore {
            total_columns: column_type_list.len(),
            _headers: vec![],
            data,
            // Column Indexes that refer to Result Variable
            result_index,
            results,
            total_rows,
        }
    }

    /// Returns (Rows * Columns)
    pub fn dim(&self) -> (usize, usize) {
        (self.total_rows, self.total_columns)
    }

    pub fn grow_forest(&self) {
        let weight_lock = RwLock::new(vec![1_f32; self.total_rows]);
        let mut full_forest: Vec<Branch> = Vec::with_capacity(N_ROUNDS * TREE_PER_ROUND);
        for _ in 0..N_ROUNDS {
            let current_weights = weight_lock.read().unwrap();
            let tree_vector: Vec<Branch> = (0..TREE_PER_ROUND)
                .collect::<Vec<usize>>()
                .par_iter()
                .map(|_| self.plant_tree(&current_weights).grow(&self))
                .collect();
            drop(current_weights);
            full_forest.extend(tree_vector);
            let (res, weights) = evaluate_tree(full_forest.clone(), &self);
            dbg!(res);

            {
                let mut writer = weight_lock.write().unwrap();
                for (element, new_weight) in writer.iter_mut().zip(weights.iter()) {
                    *element += *new_weight
                }
            }
        }
    }

    pub fn plant_tree(&self, weights: &[f32]) -> Tree {
        Tree::new(self.total_rows, self.total_columns, weights)
    }
}

pub enum ElementType {
    Float,
    Ordinal,
    Categorical,
    Boolean,
}

#[derive(Debug, Clone)]
struct LikelyColumnType {
    ordinal: usize,
    float: usize,
    character: usize,
    boolean: usize,
}

pub fn infer_column_types(file_name: String) -> Result<Vec<ElementType>, io::Error> {
    let f = File::open(file_name)?;
    let mut f = BufReader::new(f);
    let mut header_line = String::new();
    let mut row_count = 1;

    // Pull first line for header - If it's actual data we can ignore it.
    let _ = f.read_line(&mut header_line);
    let mut first_data_line = String::new();
    let _initial_line = f.read_line(&mut first_data_line);
    let initial_elements = first_data_line.split(',');
    let columns = initial_elements.count();

    // Guess at rough number of columns to assign types for
    let mut likely_column_type: Vec<LikelyColumnType> = (0_usize..columns)
        .collect::<Vec<usize>>()
        .iter()
        .map(|_| LikelyColumnType {
            ordinal: 0,
            float: 0,
            character: 0,
            boolean: 0,
        })
        .collect();

    for line in f.lines() {
        row_count += 1;
        if row_count > ROW_SAMPLING {
            break;
        }
        // Cloning to allow for printing the entire line if the evaluation fails.
        let unwrapped_line = line.unwrap();
        for (column_count, elem) in unwrapped_line.clone().split(',').enumerate() {
            if column_count > columns {
                panic!(
                    "Columns longer than expected on line {} \n {}",
                    row_count, unwrapped_line
                );
            }
            if NUMERIC_REGEX.is_match(elem) {
                likely_column_type[column_count].ordinal += 1;
            }
            if FLOAT_REGEX.is_match(elem) {
                likely_column_type[column_count].float += 1;
            }
            if CHARACTER_REGEX.is_match(elem) {
                likely_column_type[column_count].character += 1;
            }
            if BOOLEAN_REGEX.is_match(elem) {
                likely_column_type[column_count].boolean += 1;
            }
        }
    }

    let column_types: Vec<ElementType> = likely_column_type
        .iter()
        .map(move |column_summaries| {
            if column_summaries.float + column_summaries.ordinal > 0 {
                // TODO - Add in HashSet to Calculate Categorical against Ordinal -
                // If there aren't a lot of different values, likely Categorical.
                if column_summaries.float > 0 {
                    ElementType::Float
                } else {
                    ElementType::Ordinal
                }
            } else if column_summaries.boolean > ROW_SAMPLING / 2 {
                ElementType::Boolean
            } else {
                dbg!(column_summaries.float);
                dbg!(column_summaries.ordinal);
                dbg!(column_summaries.boolean);
                dbg!(column_summaries.character);
                panic!("Unknown Column Type")
            }
        })
        .collect();
    Ok(column_types)
}
