use fnv::FnvHashSet;

use tree_components::branch::{DataViewPerf, FindPurity, PossibleBranch};
use tree_components::data_store::{ColumnType, DataStore};
use tree_components::tree::{Branch, Node, RowIdxMapping, Tree};
use utils::median::NumericAggregation;

const MINIMUM_NODE_SIZE: usize = 100;
const MINIMUM_SKETCH_SIZE: usize = 100;

pub struct DataLayer<'a, 'b> {
    data_store_reference: &'a DataStore,
    tree: &'b Tree,
    rows_in_layer: Vec<usize>,
}

impl<'a, 'b> DataLayer<'a, 'b> {
    // Review Sketch Length
    pub fn new(
        data_store_reference: &'a DataStore,
        tree_reference: &'b Tree,
        rows_in_layer: Vec<usize>,
    ) -> DataLayer<'a, 'b> {
        // Data Store Borrowed will not guarantee the number of rows, that will be the former part
        // of a DataSplit.

        let initial_split_length = rows_in_layer.len();

        /*  Sketch Length isn't currently well defined. Realistically we need a way to pull a
            small number of rows without just defaulting to 400 each time. I'm going to wait for a
            bigger dataset to test on before picking a realistic way to pick sketch length.
        */

        let rows_to_keep = if initial_split_length < MINIMUM_SKETCH_SIZE {
            initial_split_length
        } else {
            std::cmp::max(initial_split_length / 2, MINIMUM_SKETCH_SIZE)
        };

        let row_index_slice = indexes_in_split(
            rows_to_keep,
            &tree_reference.row_sample_ordered,
            &rows_in_layer,
        );

        DataLayer {
            data_store_reference,
            tree: tree_reference,
            rows_in_layer: row_index_slice,
        }
    }

    fn spawn_child(&self, row_indexes: Vec<usize>) -> DataLayer<'a, 'b> {
        Self::new(self.data_store_reference, self.tree, row_indexes)
    }

    /// Return a completed Tree from an initial layer
    /// # Arguments
    /// * `node_pure` - Allows for early completion of a set of branches if
    /// all results from the Branch are of the same value.
    /// @notes
    /// This function acts recursively, and node_pure allows for early completion.
    pub fn evaluate_branches(&self, node_pure: bool) -> Branch {
        let res = self
            .data_store_reference
            .data
            .iter()
            .zip(0_usize..(self.data_store_reference.total_columns - 1))
            .map(|(column, column_index)| {
                (*column).branch_from_layer(
                    &self.data_store_reference.results,
                    &self.rows_in_layer,
                    column_index,
                )
            })
            .max_by(|x, y| x.impurity.partial_cmp(&y.impurity).unwrap())
            .unwrap();

        let (split_left, split_right) = res.splits.release();

        let split_left_len = split_left.len();
        let split_right_len = split_right.len();

        if !node_pure
            && (split_left_len > MINIMUM_NODE_SIZE)
            && (split_right_len > MINIMUM_NODE_SIZE)
        {
            Branch::NextNode(Node {
                col_idx: res.col_idx,
                bounds: res.bounds,
                branch_left: Box::new(
                    self.spawn_child(split_left)
                        .evaluate_branches(res.impurity < std::f32::EPSILON),
                ),
                branch_right: Box::new(
                    self.spawn_child(split_right)
                        .evaluate_branches(res.impurity < std::f32::EPSILON),
                ),
            })
        } else {
            Branch::Leaf(match &self.data_store_reference.results {
                ColumnType::Boolean(results_column) => {
                    let mut true_results: usize = 0_usize;
                    for row_index in self.rows_in_layer.iter() {
                        if results_column[*row_index] {
                            true_results += 1;
                        }
                    }
                    RowIdxMapping::Boolean(true_results > (self.rows_in_layer.len() / 2))
                }
                ColumnType::Categorical(_results_column) => unimplemented!(),
                ColumnType::Float(results_column) => RowIdxMapping::Float(
                    results_column.sketched_median(self.rows_in_layer.len(), &self.rows_in_layer),
                ),
                ColumnType::Ordinal(results_column) => RowIdxMapping::Ordinal(
                    results_column.sketched_median(self.rows_in_layer.len(), &self.rows_in_layer),
                ),
            })
        }
    }
}

impl ColumnType {
    fn branch_from_layer(
        &self,
        result_data: &ColumnType,
        sketch_indexes: &[usize],
        column_index: usize,
    ) -> DataViewPerf {
        match self {
            // TODO - Refactor out the clone
            ColumnType::Float(column_data) => PossibleBranch::new(
                &ColumnType::Float(column_data.clone()),
                column_index,
                result_data,
                sketch_indexes,
            )
                .find_purest_split(),
            ColumnType::Categorical(column_data) => PossibleBranch::new(
                &ColumnType::Categorical(column_data.clone()),
                column_index,
                result_data,
                sketch_indexes,
            )
                .find_purest_split(),
            ColumnType::Ordinal(column_data) => PossibleBranch::new(
                &ColumnType::Ordinal(column_data.clone()),
                column_index,
                result_data,
                sketch_indexes,
            )
                .find_purest_split(),
            ColumnType::Boolean(column_data) => PossibleBranch::new(
                &ColumnType::Boolean(column_data.clone()),
                column_index,
                result_data,
                sketch_indexes,
            )
                .find_purest_split(),
        }
    }
}

/// Return a Vector of the first split_length indexes in target_indexes from available data.
///
/// # Arguments
/// ```split_length``` - Number of indexes to retrieve
/// ```target_indexes``` - All indexes to retrieve
/// ```available_data``` - All available indexes
fn indexes_in_split(
    split_length: usize,
    target_indexes: &[usize],
    available_data: &[usize],
) -> Vec<usize> {
    if split_length > available_data.len() {
        panic!("More data requested than available")
    }

    let mut target_split: FnvHashSet<usize> =
        FnvHashSet::with_capacity_and_hasher(split_length, Default::default());
    {
        for elem in target_indexes {
            target_split.insert(*elem);
        }
    }

    let mut row_index_slice: Vec<usize> = Vec::with_capacity(split_length);
    let mut row_incl_count: usize = 0;

    for row_idx in available_data {
        if target_split.contains(&row_idx) {
            row_index_slice.push(*row_idx);
            row_incl_count += 1;
        }
        if row_incl_count >= split_length {
            break;
        };
    }

    row_index_slice
}
