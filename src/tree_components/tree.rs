use std::cmp::Ordering;
use std::cmp::Ordering::Equal;

use rand::Rng;
use rand_core::SeedableRng;
use rand_xoshiro::Xoshiro256Plus;

use evaluation::evaluators::{Evaluation, TreeResult};
use tree_components::data_store::{ColumnType, DataStore};
use tree_components::layer::DataLayer;

const ROW_SAMPLING_PERCENTAGE: f32 = 0.75;
const COLUMN_SAMPLING_PERCENTAGE: f32 = 0.995;

#[derive(Clone)]
pub enum RowIdxMapping {
    Float(f32),
    Ordinal(i32),
    Categorical(usize),
    Boolean(bool),
}

impl PartialOrd for RowIdxMapping {
    fn partial_cmp(&self, other: &RowIdxMapping) -> Option<Ordering> {
        match self {
            RowIdxMapping::Float(elem) => match other {
                RowIdxMapping::Float(other_elem) => {
                    Some(elem.partial_cmp(other_elem).unwrap_or(Equal))
                }
                _ => unreachable!(),
            },
            RowIdxMapping::Ordinal(elem) => match other {
                RowIdxMapping::Ordinal(other_elem) => Some(elem.cmp(other_elem)),
                _ => unreachable!(),
            },
            RowIdxMapping::Categorical(elem) => match other {
                RowIdxMapping::Categorical(other_elem) => Some(elem.cmp(other_elem)),
                _ => unreachable!(),
            },
            RowIdxMapping::Boolean(elem) => match other {
                RowIdxMapping::Boolean(other_elem) => Some(elem.cmp(other_elem)),
                _ => unreachable!(),
            },
        }
    }
}

impl PartialEq for RowIdxMapping {
    fn eq(&self, other: &RowIdxMapping) -> bool {
        match self {
            RowIdxMapping::Float(elem) => match other {
                RowIdxMapping::Float(other_elem) => (elem - other_elem).abs() <= std::f32::EPSILON,
                _ => unreachable!(),
            },
            RowIdxMapping::Ordinal(elem) => match other {
                RowIdxMapping::Ordinal(other_elem) => elem == other_elem,
                _ => unreachable!(),
            },
            RowIdxMapping::Categorical(elem) => match other {
                RowIdxMapping::Categorical(other_elem) => elem == other_elem,
                _ => unreachable!(),
            },
            RowIdxMapping::Boolean(elem) => match other {
                RowIdxMapping::Boolean(other_elem) => elem == other_elem,
                _ => unreachable!(),
            },
        }
    }
}

type Link = Box<Branch>;

#[derive(Clone)]
pub enum Branch {
    NextNode(Node),
    Leaf(RowIdxMapping),
}

#[derive(Clone)]
pub struct Node {
    pub col_idx: usize,
    pub bounds: RowIdxMapping,
    pub branch_left: Link,
    pub branch_right: Link,
}

pub trait NodeLookup {
    fn within_bounds(&self, data_store_reference: &DataStore, index: usize) -> bool;
}

impl NodeLookup for Node {
    fn within_bounds(&self, data_store_reference: &DataStore, index: usize) -> bool {
        match self.bounds {
            RowIdxMapping::Float(cond) => match &data_store_reference.data[self.col_idx] {
                ColumnType::Float(column_data) => column_data[index] >= cond,
                _ => panic!("Expected float"),
            },
            RowIdxMapping::Ordinal(cond) => match &data_store_reference.data[self.col_idx] {
                ColumnType::Ordinal(column_data) => column_data[index] >= cond,
                _ => panic!("Expected ordinal"),
            },
            RowIdxMapping::Boolean(cond) => match &data_store_reference.data[self.col_idx] {
                ColumnType::Boolean(column_data) => !column_data[index] & !cond,
                _ => panic!("Expected bool"),
            },
            RowIdxMapping::Categorical(_) => unimplemented!(),
        }
    }
}

impl Node {
    /// Evaluate a row according to a decision tree, returning a prediction
    ///
    /// # Arguments
    /// *`data_store_reference` Reference to a DataStore
    /// *`row` Reference to a slice of row elements
    ///
    /// # Returns
    /// A single element of ResultType
    pub fn lookup_element(&self, data_store_reference: &DataStore, index: usize) -> RowIdxMapping {
        if self.within_bounds(data_store_reference, index) {
            match *self.branch_left {
                Branch::NextNode(ref node) => node.lookup_element(data_store_reference, index),
                Branch::Leaf(ref result) => result.clone(),
            }
        } else {
            match *self.branch_right {
                Branch::NextNode(ref node) => node.lookup_element(data_store_reference, index),
                Branch::Leaf(ref result) => result.clone(),
            }
        }
    }
}

/// Provide a sampling probability for each row
///
/// # Arguments
///
/// *`total_rows` - usize Total Rows in DataSet to provide probability for
/// *`weights` - Weights representing the number of errors from the last tree run
///
/// # Returns
/// Tuple -
///     Vec<usize> - Indexes sorted randomly for Data Sketches within Layer to minimise work required to
///         accurately estimate characteristics like mean, standard deviation, etc.
///     Vec<usize>  - Indexes sorted by a weighted random probability for Row Sampling
pub fn row_sampling(total_rows: usize, weights: &[f32]) -> (Vec<usize>, Vec<usize>) {
    let mut rng = Xoshiro256Plus::from_rng(rand::rngs::EntropyRng::new()).unwrap();
    let data_sketch_probability = (0..total_rows)
        .map(|_| rng.gen_range::<f32, f32, f32>(0_f32, 100_f32))
        .collect::<Vec<f32>>();

    let row_sampling_probability = data_sketch_probability
        .clone()
        .iter()
        .zip(weights)
        .map(|(initial_weight, weight)| initial_weight + weight)
        .collect::<Vec<f32>>();

    let mut data_sketch_probability: Vec<(f32, usize)> = data_sketch_probability
        .into_iter()
        .enumerate()
        .map(|(idx, v): (usize, f32)| (v, idx))
        .collect();

    let mut row_sampling_probability: Vec<(f32, usize)> = row_sampling_probability
        .into_iter()
        .enumerate()
        .map(|(idx, v): (usize, f32)| (v, idx))
        .collect();

    // Sorts greatest to smallest
    data_sketch_probability.sort_unstable_by(|a, b| b.0.partial_cmp(&a.0).unwrap());

    row_sampling_probability.sort_unstable_by(|a, b| b.0.partial_cmp(&a.0).unwrap());

    let data_sketch_ordering: Vec<usize> = data_sketch_probability.iter().map(|t| t.1).collect();
    let row_sampling_ordering: Vec<usize> = row_sampling_probability.iter().map(|t| t.1).collect();

    (data_sketch_ordering, row_sampling_ordering)
}

#[derive(Clone)]
pub struct Tree {
    pub data_sketch_ordered: Vec<usize>,
    pub row_sample_ordered: Vec<usize>,
    pub columns: Vec<usize>,
}

impl Tree {
    pub fn new(total_rows: usize, total_cols: usize, weights: &[f32]) -> Tree {
        let (data_sketch_order, row_sampling_order) = row_sampling(total_rows, weights);

        let mut rng = Xoshiro256Plus::from_rng(rand::rngs::EntropyRng::new()).unwrap();

        let mut col_sampling_probability = (0..(total_cols - 1))
            .map(|col_idx| (rng.gen::<f32>(), col_idx))
            .collect::<Vec<(f32, usize)>>();

        col_sampling_probability.sort_unstable_by(|a, b| b.0.partial_cmp(&a.0).unwrap());

        let col_sampling_probability: Vec<usize> =
            col_sampling_probability.iter().map(|t| t.1).collect();

        let columns_to_sample = (COLUMN_SAMPLING_PERCENTAGE * (total_cols as f32)) as usize;

        let col_sampling_probability = col_sampling_probability[0..columns_to_sample].to_vec();

        Tree {
            data_sketch_ordered: data_sketch_order,
            row_sample_ordered: row_sampling_order,
            columns: col_sampling_probability,
        }
    }

    pub fn grow(&self, data_store: &DataStore) -> Branch {
        let row_sample_length = (data_store.total_rows as f32 * ROW_SAMPLING_PERCENTAGE) as usize;

        let row_sample_data = self.row_sample_ordered[0..row_sample_length].to_vec();

        DataLayer::new(data_store, self, row_sample_data).evaluate_branches(false)
    }
}

/// Evaluate a Set of Decision Trees
///
/// # Arguments
///
/// *`forest` - Vec<Tree> All trees in the current forest
/// *`data_store` - DataStore containing all the formatted information
///
/// # Returns
/// Tuple -
///     f64 - Score for the forest so far
///     Vec<f32> - Weights from the forest results
pub fn evaluate_tree(forest: Vec<Branch>, data_store: &DataStore) -> (f64, Vec<f32>) {
    match &data_store.results {
        ColumnType::Boolean(results_column) => {
            TreeResult::new(data_store, results_column, forest).evaluate()
        }
        ColumnType::Categorical(results_column) => {
            TreeResult::new(data_store, results_column, forest).evaluate()
        }
        ColumnType::Float(results_column) => {
            TreeResult::new(data_store, results_column, forest).evaluate()
        }
        ColumnType::Ordinal(results_column) => {
            TreeResult::new(data_store, results_column, forest).evaluate()
        }
    }
}
