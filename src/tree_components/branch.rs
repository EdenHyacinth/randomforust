use std::cmp::Ordering::Equal;
use std::collections::HashMap;

use maths::histogram::*;
use tree_components::data_store::ColumnType;
use tree_components::tree::RowIdxMapping;
use utils::median::NumericAggregation;

/// Non-Copy View into a single Column of DataStore
pub struct PossibleBranch<'a> {
    pub data: &'a ColumnType,
    pub results: &'a ColumnType,
    pub column_index: usize,
    pub branch_indices: &'a [usize],
    // TODO - Add in Sketch Indices for Histogram Construction
    pub histogram_bins: Option<ColumnType>,
}

impl<'a> PossibleBranch<'a> {
    pub fn new(
        data: &'a ColumnType,
        column_index: usize,
        results: &'a ColumnType,
        branch_indices: &'a [usize],
    ) -> Self {
        PossibleBranch {
            data,
            results,
            column_index,
            branch_indices,
            histogram_bins: None,
        }
    }
}

/// InSplit, OutofSplit
pub struct DataSplit {
    pub split_retained: Vec<usize>,
    pub split_dropped: Vec<usize>,
}
impl DataSplit {
    pub fn new(split_retained: Vec<usize>, split_dropped: Vec<usize>) -> DataSplit {
        DataSplit {
            split_retained,
            split_dropped,
        }
    }
    pub fn release(self) -> (Vec<usize>, Vec<usize>) {
        (self.split_retained, self.split_dropped)
    }
}

/// Find Purity of a Branch
///
/// Uses Information Gain to calculate how well a branch of float can split any given set
/// of Results, so long as the Results can be compared into greater/smaller.
pub trait FindPurity {
    fn find_purest_split(&self) -> DataViewPerf;
}

impl<'a> FindPurity for PossibleBranch<'a> {
    fn find_purest_split(&self) -> DataViewPerf {
        // TODO - Move upwards - results apply to all possible branches.
        let (result_min, result_max) = match self.results {
            ColumnType::Boolean(results_column) => {
                match (
                    self.branch_indices.iter().any(|index| results_column[*index]),
                    self.branch_indices.iter().any(|index| !results_column[*index]),
                ) {
                    (true, true) => (RowIdxMapping::Boolean(false), RowIdxMapping::Boolean(true)),
                    (true, false) => (RowIdxMapping::Boolean(true), RowIdxMapping::Boolean(true)),
                    (false, true) => (RowIdxMapping::Boolean(false), RowIdxMapping::Boolean(false)),
                    (false, false) => unreachable!(),
                }
            }
            ColumnType::Categorical(_results_column) => (unimplemented!()),
            ColumnType::Float(results_column) => {
                let mut local_results = self
                    .branch_indices
                    .iter()
                    .map(|index| results_column[*index])
                    .collect::<Vec<f32>>();
                local_results.sort_by(|a, b| a.partial_cmp(b).unwrap_or(Equal));
                (
                    RowIdxMapping::Float(local_results[0]),
                    RowIdxMapping::Float(local_results[local_results.len()]),
                )
            }
            ColumnType::Ordinal(results_column) => (
                RowIdxMapping::Ordinal(
                    match self.branch_indices
                        .iter()
                        .map(|index| results_column[*index])
                        .min() {
                        Some(min_result) => min_result,
                        None => panic!("Min failed on Ordinal Result")
                    }),
                RowIdxMapping::Ordinal(
                    match self.branch_indices
                        .iter()
                        .map(|index| results_column[*index])
                        .max() {
                        Some(max_result) => max_result,
                        None => panic!("Max failed on Ordinal Result")
                    }
                )
            )
        };
        let result_bounds = (result_min, result_max);
        // END TODO

        let (histogram_details, bin_frequency_by_row) = match self.data {
            ColumnType::Float(column_data) => {
                let (min_bound, max_bound) = column_data.min_max(&self.branch_indices);
                column_data.histogram(
                    &self.branch_indices,
                    (
                        RowIdxMapping::Float(min_bound),
                        RowIdxMapping::Float(max_bound),
                    ),
                )
            }
            ColumnType::Ordinal(column_data) => {
                let (min_bound, max_bound) = column_data.min_max(&self.branch_indices);
                column_data.histogram(
                    &self.branch_indices,
                    (
                        RowIdxMapping::Ordinal(min_bound),
                        RowIdxMapping::Ordinal(max_bound),
                    ),
                )
            }
            ColumnType::Boolean(column_data) => column_data.histogram(
                &self.branch_indices,
                (RowIdxMapping::Boolean(false), RowIdxMapping::Boolean(true)),
            ),
            ColumnType::Categorical(_column_data) => {
                unimplemented!()
                /*
                    Average Results Value by Group
                    Order by Average Results Value
                    Count Values in each Group
                    Minimise Variance of Average Results Value per Strata by using
                    w = Value Count
                    a = Mean Value in Group
                    Minimise by continuous grouping (10 -> 38)
                    all_contig_seq <- function(elems, groups) {
                    +   if (groups == 1) {
                    +     return(paste0(elems, collapse = ","))
                    +   } else {
                    +     unlist(lapply(1:(length(elems) - groups + 1), function(i){
                    +       hol <- paste0(head(elems, i), collapse = ",")
                    +       rol <- paste0(all_contig_seq(
                    +         tail(elems, (length(elems) - i)), groups = (groups - 1)
                    +       ))
                    +       paste0(hol, "|", rol)
                    +     }))
                    +   }
                    + }
                */
            }
        };

        let (results_histogram_details, result_bin_frequency_by_row) = match self.results {
            ColumnType::Boolean(results_column) => {
                (*results_column).histogram(&self.branch_indices, result_bounds)
            }
            ColumnType::Categorical(_results_column) => unimplemented!(),
            ColumnType::Float(results_column) => {
                (*results_column).histogram(&self.branch_indices, result_bounds)
            }
            ColumnType::Ordinal(results_column) => {
                (*results_column).histogram(&self.branch_indices, result_bounds)
            }
        };

        let histogram_bin_details = histogram_details.bins.clone();

        let (max_split_score_index, rows_in_split, rows_out_split, additional_information) =
            score_information_gain(
                bin_frequency_by_row,
                histogram_details,
                result_bin_frequency_by_row,
                results_histogram_details,
                self.branch_indices.len(),
                &self.branch_indices,
            );

        DataViewPerf::new(
            histogram_bin_details[max_split_score_index].clone(),
            DataSplit::new(rows_in_split, rows_out_split),
            additional_information as f32,
            self.column_index,
        )
    }
}

pub struct DataViewPerf {
    pub bounds: RowIdxMapping,
    pub splits: DataSplit,
    pub impurity: f32,
    pub col_idx: usize,
}

impl DataViewPerf {
    pub fn new(
        bounds: RowIdxMapping,
        splits: DataSplit,
        gini_impurity: f32,
        col_idx: usize,
    ) -> DataViewPerf {
        DataViewPerf {
            bounds,
            splits,
            impurity: gini_impurity,
            col_idx,
        }
    }
}

fn score_information_gain<T, ResultType>(
    target_bin_frequency_by_row: Vec<(f32, usize)>,
    target_histogram_details: HistogramDetails<T>,
    result_bin_frequency_by_row: Vec<(f32, usize)>,
    result_histogram_details: HistogramDetails<ResultType>,
    sketch_rows: usize,
    rows_in_sketch: &[usize],
) -> (usize, Vec<usize>, Vec<usize>, f64) {
    let split_score_by_bin = (0..(target_histogram_details.number_of_bins - 1))
        .collect::<Vec<usize>>()
        .into_iter()
        .map(|split_bin_idx| {
            let mut joined_histogram_frequency: HashMap<(bool, usize), usize> = HashMap::new();
            let mut result_bin_frequency: Vec<f32> =
                vec![0.0_f32; result_histogram_details.number_of_bins];
            let mut lhs_out_split_frequency: usize = 0;
            let mut lhs_in_split_frequency: usize = 0;
            for ((_, predictor_bin), (_, results_bin)) in target_bin_frequency_by_row
                .iter()
                .zip(result_bin_frequency_by_row.clone())
                {
                    result_bin_frequency[results_bin] += 1.0_f32;
                    if *predictor_bin > split_bin_idx {
                        lhs_in_split_frequency += 1;
                    } else {
                        lhs_out_split_frequency += 1;
                    }
                    joined_histogram_frequency
                        .entry((*predictor_bin > split_bin_idx, results_bin))
                        .and_modify(|e| *e += 1_usize)
                        .or_insert(1_usize);
                }

            joined_histogram_frequency
                .keys()
                .fold(0.0f64, |acc, (lhs_bin, rhs_bin)| {
                    let joint_prob = match joined_histogram_frequency.get(&(*lhs_bin, *rhs_bin)) {
                        Some(frequency) => *frequency as f64 / sketch_rows as f64,
                        None => panic!("No Key found for Frequency"),
                    };
                    let lhs_prob = (if *lhs_bin {
                        lhs_in_split_frequency
                    } else {
                        lhs_out_split_frequency
                    } as f64)
                        / sketch_rows as f64;
                    let rhs_prob = f64::from(result_bin_frequency[*rhs_bin]) / sketch_rows as f64;
                    acc + joint_prob * (joint_prob / (lhs_prob * rhs_prob)).log(2.0)
                })
        })
        .collect::<Vec<f64>>();
    let initial_score = split_score_by_bin[0];
    let max_split_score = split_score_by_bin
        .clone()
        .into_iter()
        .skip(1)
        .fold(std::f64::MIN, f64::max);

    let max_split_score_index = match split_score_by_bin
        .iter()
        .position(|&i| (i - max_split_score).abs() <= std::f64::EPSILON)
        {
        Some(index) => index,
            None => panic!("Not Found"),
    };

    let mut rows_in_split: Vec<usize> = Vec::with_capacity(rows_in_sketch.len());
    let mut rows_out_split: Vec<usize> = Vec::with_capacity(rows_in_sketch.len());

    for (idx, (_, bin_index)) in target_bin_frequency_by_row.iter().enumerate() {
        if *bin_index > max_split_score_index {
            rows_in_split.push(rows_in_sketch[idx])
        } else {
            rows_out_split.push(rows_in_sketch[idx])
        }
    }

    (
        max_split_score_index,
        rows_in_split,
        rows_out_split,
        max_split_score - initial_score,
    )
}
