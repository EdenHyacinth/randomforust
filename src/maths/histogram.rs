use tree_components::tree::RowIdxMapping;
use utils::column_tools::fold_data;

pub struct HistogramDetails<T> {
    pub bins: T,
    pub number_of_bins: usize,
}

pub trait Histogram {
    /// Create a Histogram of Elements
    ///
    /// # Arguments
    /// * `indexes` - Ref to Row Indexes
    /// * `index_length` - Length of Row Index
    /// * `column_index`
    /// * `bounds`(T, T) - Min and Max Values of the Data Provided
    ///
    /// # Returns
    /// HistogramDetails<T>
    /// Bins - Number of Bins - Row IDXs includes
    ///
    /// Vec<(f32, usize)> - Bin Frequency & Bin Index for each Row
    fn histogram(
        &self,
        indexes: &[usize],
        bounds: (RowIdxMapping, RowIdxMapping),
    ) -> (HistogramDetails<Vec<RowIdxMapping>>, Vec<(f32, usize)>);
}

impl Histogram for Vec<f32> {
    fn histogram(
        &self,
        indexes: &[usize],
        bounds: (RowIdxMapping, RowIdxMapping),
    ) -> (HistogramDetails<Vec<RowIdxMapping>>, Vec<(f32, usize)>) {
        let mean = fold_data(self, indexes, |a, b| a + b, || 0.0) / indexes.len() as f32;

        let standard_deviation = (fold_data(
            self,
            indexes,
            |a, b| {
                let x = b - mean;
                a + x * x
            },
            || 0.0,
        ) / indexes.len() as f32)
            .sqrt();

        // Using Scott's Normal Reference Rule for bin selection
        let bin_width =
            ((standard_deviation * 3.5) / (indexes.len() as f32).powf(1.0 / 3.0)) as f32;
        let (min_val, max_val) = match bounds {
            (RowIdxMapping::Float(min), RowIdxMapping::Float(max)) => (min, max),
            _ => panic!("Expected Float from Bounds"),
        };
        let est_bins: usize = ((max_val - min_val) / bin_width) as usize + 1;

        let histogram_parts = (0_usize..=(est_bins))
            .map(|i| min_val + bin_width * i as f32)
            .map(RowIdxMapping::Float)
            .collect::<Vec<RowIdxMapping>>();

        let mut bin_frequency: Vec<f32> = vec![0.0_f32; est_bins];
        let mut row_histogram: Vec<usize> = Vec::with_capacity(self.len());

        for index in indexes {
            let histogram_bin = find_bucket(&histogram_parts, RowIdxMapping::Float(self[*index]));
            row_histogram.push(histogram_bin);
            bin_frequency[histogram_bin] += 1.0_f32;
        }

        (
            HistogramDetails {
                bins: histogram_parts,
                number_of_bins: est_bins,
            },
            row_histogram
                .iter()
                .map(|idx| (bin_frequency[*idx], *idx))
                .collect::<Vec<(f32, usize)>>(),
        )
    }
}

impl Histogram for Vec<i32> {
    fn histogram(
        &self,
        indexes: &[usize],
        bounds: (RowIdxMapping, RowIdxMapping),
    ) -> (HistogramDetails<Vec<RowIdxMapping>>, Vec<(f32, usize)>) {
        let mean = fold_data(self, indexes, |a, b| a + b, || 0_i32) as f32 / indexes.len() as f32;

        let standard_deviation = ((fold_data(
            self,
            indexes,
            |a, b| {
                let x = b - mean as i32;
                a + x * x
            },
            || 0_i32,
        ) as f32)
            / indexes.len() as f32)
            .sqrt();

        // Using Scott's Normal Reference Rule for bin selection
        let bin_width =
            ((standard_deviation * 3.5) / (indexes.len() as f32).powf(1.0 / 3.0)) as i32;
        let (min_val, max_val) = match bounds {
            (RowIdxMapping::Ordinal(min), RowIdxMapping::Ordinal(max)) => (min, max),
            _ => panic!("Expected Ordinal from Bounds"),
        };
        let est_bins: usize = ((max_val - min_val) / bin_width) as usize + 1;

        let histogram_parts = (0_i32..=(est_bins as i32))
            .map(|i| min_val + bin_width * i)
            .map(RowIdxMapping::Ordinal)
            .collect::<Vec<RowIdxMapping>>();

        let mut bin_frequency: Vec<f32> = vec![0.0_f32; est_bins];
        let mut row_histogram: Vec<usize> = Vec::with_capacity(self.len());

        for index in indexes {
            let histogram_bin = find_bucket(&histogram_parts, RowIdxMapping::Ordinal(self[*index]));
            row_histogram.push(histogram_bin);
            bin_frequency[histogram_bin] += 1.0_f32;
        }

        (
            HistogramDetails {
                bins: histogram_parts,
                number_of_bins: est_bins,
            },
            row_histogram
                .iter()
                .map(|idx| (bin_frequency[*idx], *idx))
                .collect::<Vec<(f32, usize)>>(),
        )
    }
}

impl Histogram for Vec<bool> {
    fn histogram(
        &self,
        indexes: &[usize],
        _bounds: (RowIdxMapping, RowIdxMapping),
    ) -> (HistogramDetails<Vec<RowIdxMapping>>, Vec<(f32, usize)>) {
        let est_bins: usize = 2;
        let histogram_parts = vec![RowIdxMapping::Boolean(false), RowIdxMapping::Boolean(true)];

        let mut bin_frequency: Vec<f32> = vec![0.0_f32; est_bins];
        let mut row_histogram: Vec<usize> = Vec::with_capacity(indexes.len());

        for index in indexes {
            let histogram_bin = if self[*index] { 1 } else { 0 };
            row_histogram.push(histogram_bin);
            bin_frequency[histogram_bin] += 1.0_f32;
        }

        (
            HistogramDetails {
                bins: histogram_parts,
                number_of_bins: est_bins,
            },
            row_histogram
                .iter()
                .map(|idx| (bin_frequency[*idx], *idx))
                .collect::<Vec<(f32, usize)>>(),
        )
    }
}

// TODO - Poss Unused?
fn find_bucket<T>(data: &[T], elem: T) -> usize
    where
        T: std::cmp::PartialOrd,
{
    let index_guess = (data.len() - 1) / 2;
    if elem >= data[index_guess]
        && (index_guess == 0 || index_guess + 1 == data.len() || elem < data[index_guess + 1])
    {
        index_guess
    } else if elem >= data[index_guess] {
        index_guess + 1 + find_bucket(&data[(index_guess + 1)..data.len()], elem)
    } else {
        find_bucket(&data[0..=index_guess], elem)
    }
}
