use tree_components::data_store::{ColumnType, DataStore, ElementType};
use utils::column_tools::fold_data;

#[test]
fn data_dim() {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );
    assert_eq!((1372, 5), all_data.dim())
}

#[test]
fn data_sum_view() {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );

    let index = (0..all_data.total_rows).collect::<Vec<usize>>();
    let res = match &all_data.data[0] {
        ColumnType::Float(column_data) => fold_data(column_data, &index, |a, b| a + b, || 0.0),
        _ => panic!("Expected Float"),
    };
    assert!((595.0847726999998 - res) <= std::f32::EPSILON);
}

#[test]
fn data_mean() {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );

    let index = (0..all_data.total_rows).collect::<Vec<usize>>();
    let mean = match &all_data.data[0] {
        ColumnType::Float(column_data) => {
            fold_data(column_data, &index, |a, b| a + b, || 0.0) / index.len() as f32
        }
        _ => panic!("Expected Float"),
    };
    assert!((0.4337352570699707 - mean) <= ::std::f32::EPSILON);

    let mean = match &all_data.data[1] {
        ColumnType::Float(column_data) => {
            fold_data(column_data, &index, |a, b| a + b, || 0.0) / index.len() as f32
        }
        _ => panic!("Expected Float"),
    };
    assert!((1.9223 - mean) <= ::std::f32::EPSILON);
}

#[test]
fn standard_deviation() {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let all_data: DataStore = DataStore::new(
        vec![
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Float,
            ElementType::Boolean,
        ],
        4,
        file_name.to_string(),
        false,
    );
    let index = (0..all_data.total_rows).collect::<Vec<usize>>();

    let mean = match &all_data.data[0] {
        ColumnType::Float(column_data) => {
            fold_data(column_data, &index, |a, b| a + b, || 0.0) / index.len() as f32
        }
        _ => panic!("Expected Float"),
    };

    let standard_deviation = match &all_data.data[0] {
        ColumnType::Float(column_data) => (fold_data(
            column_data,
            &index,
            |a, b| {
                let x = b - mean;
                a + x * x
            },
            || 0.0,
        ) / index.len() as f32)
            .sqrt(),
        _ => panic!("Expected Float"),
    };

    assert!((2.841726405239482 - standard_deviation) <= std::f32::EPSILON);
}
