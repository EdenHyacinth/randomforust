extern crate criterion;
extern crate fnv;
extern crate lazy_static;
extern crate rand;
extern crate rand_core;
extern crate rand_xoshiro;
extern crate rayon;
extern crate regex;

pub mod evaluation;
mod maths;
pub mod tree_components;
pub mod utils;

#[cfg(test)]
mod tests;
