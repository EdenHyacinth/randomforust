extern crate lyssarian_forest;
extern crate rayon;

use lyssarian_forest::tree_components::data_store::{DataStore, infer_column_types};

fn main() {
    let file_name = "data_examples/data_banknote_authentication_bool.csv";
    let result_index = 4;
    let column_types = infer_column_types(file_name.to_string()).unwrap();
    DataStore::new(column_types, result_index, file_name.to_string(), false).grow_forest();
}
